import boto.ses
import pymongo
from bson.objectid import ObjectId
from datetime import datetime, timedelta
import time

import babel.numbers
import decimal

from libs.engine import render_from_template

DB_URL = 'mongodb://172.31.16.164/'

# DB_HOST = '172.31.16.164'
DB_HOST = 'localhost'
DB_PORT = 27017

SENDER_MASS_EMAIL = 'mohitagarwal.designbids@gmail.com'

SMS_API = 'http://subsms.obligr.com/api/pushsms.php/?'

# AWS Connection
AmazonConnection = boto.ses.connect_to_region(
    'us-west-2',
    aws_access_key_id='AKIAJNGUFFF4BHRWZTZQ',
    aws_secret_access_key='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'
)


def notification_mail(**email):
    AmazonConnection.send_email(email['from'],
                                email['subject'],
                                None,
                                email['to'],
                                html_body=email['content'],
                                format='html')
    return True


def monitored_transactional_mail(**email):
    try:
        AmazonConnection.send_email(
            email['from'],
            email['subject'],
            None,
            [email['to']],
            [email['cc']],
            html_body=email['content'],
            format='html')
        return True

    except:
        print('[*] mail  To: ', email['to'], ' Subject: ', email['subject'], ' might fail!')
        return False


def compose_mail(username=None, project_title=None, link=None):
    print(' Username : ', username, "  project : ", project_title, " Link : ", link)
    fullUrl = 'https://www.designbids.in' + link
    return "Hi {user}, new applications on your project {project} on designbids. See them on {link}. Call us on 9243667702 / 03 for any queries " \
        .format(user=username, project=project_title, link=fullUrl)



def encodingObjectId(object_id):
    object_id = str(object_id)
    if object_id == '':
        return ''
    object_id = str(object_id)
    return object_id[len(object_id) - 7:len(object_id)] + object_id + object_id[0:7]



def generateEmailFromList():
    import os
    dir_path = os.path.dirname(os.path.realpath(__file__))
    try:
        client = pymongo.MongoClient(DB_HOST, DB_PORT)
    except pymongo.errors.ConnectionFailure:
        return False

    DB = client['designbidscom']
    users_coll = DB['users']
    project_coll = DB['projects']
    inactive_user_coll = DB['inactiveclients']

    TODAY = datetime.utcnow()
    NEXT_DATE = TODAY + timedelta(days=3)
    print(" The 3 days after date calculated: ", NEXT_DATE)

    query = "this.next_date_to_shoot.toJSON().slice(0, 10)==='{0}' && this.sms_sent_count > 0".format(
        TODAY.strftime('%Y-%m-%d'))
    print(" Query : ", query)

    listOfUsersToSendSMS = inactive_user_coll.find({'$where': query})

    # listOfUsersToSendSMS = [{'user': '58d380706f544b100aa751d9', 'project': '58e5df7374d8ae36dccd322d'}]
    MAILS = []
    for user in listOfUsersToSendSMS:
        userData = users_coll.find_one({'_id': ObjectId(user['user'])}, {'email': 1, 'phoneNo': 1, 'name': 1})
        projectData = project_coll.find_one({'_id': ObjectId(user['project'])})
        inactive_user_coll.update({'user': ObjectId(user['user'])}, {'$inc': {'sms_sent_count': -1},
                                                                     '$set': {'next_date_to_shoot': NEXT_DATE}})
        # print(' Project :', projectData['bids'])

        if 'bids' in projectData:
            for bid in projectData['bids']:
                if 'bidder' not in bid:
                    continue

                bid['bidder'] = users_coll.find_one({'_id': ObjectId(bid['bidder'])})
                quote = ''
                if 'quote_price' in bid['firstSubmission']:
                    quote = \
                    babel.numbers.format_currency(decimal.Decimal(bid['firstSubmission']['quote_price']), "IND").split(
                        "IND")[1]
                else:
                    quote = "NA"

                args = {
                    'bids': projectData['bids'][0:3],
                    'link': 'https://www.designbids.in/user/login/redirect-dashboard/' + encodingObjectId(
                        projectData['employer']) + '/' + str(projectData['_id']),
                    'quote': quote,
                    "services": ', '.join(bid["bidder"]["designer"]["specialization"])
                }
                # print(bid)
                # / home / ubuntu
                content = render_from_template(dir_path+'/templates/', '369.html', **args)
                print(' CONTENT : ')
            if userData is None:
                continue
            MAILS.append({'to': userData['email'], 'from': SENDER_MASS_EMAIL, 'content': content,
                          'subject': 'Hey! Your project recieved bids by designer'})
    for mail in MAILS:
        time.sleep(10)
        notification_mail(**mail)
    ADMIN = {'to': 'vishucool.venu@gmail.com',
             'from': SENDER_MASS_EMAIL, 'content': ' Routine 369 Notification Started. '+str(len(MAILS))+' are going to be notified.',
             'subject': '369 Routine! Started ... '}
    notification_mail(**ADMIN)
    return True


if __name__ == '__main__':
    generateEmailFromList()