import pymongo

# DB_HOST = '172.31.16.164'
DB_HOST = 'localhost'
DB_PORT = 27017

from urllib import urlencode
import requests

from datetime import datetime, timedelta
from bson.objectid import ObjectId

import time

SMS_API = 'http://subsms.obligr.com/api/pushsms.php/?'


def encodingObjectId(object_id):
    object_id = str(object_id)
    if object_id == '':
        return ''
    object_id = str(object_id)
    return object_id[len(object_id) - 7:len(object_id)] + object_id + object_id[0:7]


def compose_mail(username=None, project_title=None, link=None):
    print(' Username : ', username, "  project : ", project_title, " Link : ", link)
    fullUrl = 'https://www.designbids.in' + link
    return "Hi {user}, new applications on your project {project} on designbids. See them on {link}. Call us on 9243667702 / 03 for any queries " \
        .format(user=username, project=project_title, link=fullUrl)


def send_transactional_sms(sms):
    print(' SMS : ', sms)
    searchString = urlencode({'username': 'mohitdb', 'password': '21221', 'sender': 'DGBIDS'
                                 , 'message': sms['body'], 'numbers': sms['to'], 'unicode': 'false',
                              'flash': 'false'})

    print(" URL BUILT : ", searchString)
    query_string = SMS_API + searchString
    res = requests.get(query_string)
    if res.status_code == 200:
        print(' SMS sent successfully ')
        return True
    else:
        print(' SMS to ', sms.body, '  message : ', sms['body'], ' Failed ', res.status_code)
        return False


def generateSMSFromList():
    try:
        client = pymongo.MongoClient(DB_HOST, DB_PORT)
    except pymongo.errors.ConnectionFailure:
        return False

    DB = client['designbidscom']
    users_coll = DB['users']
    project_coll = DB['projects']
    inactive_user_coll = DB['inactiveclients']

    TODAY = datetime.utcnow()
    print(" TODAY IN PROCESSLISTOFINACTIVEUSER : ", TODAY)

    query = "this.next_date_to_shoot.toJSON().slice(0, 10)==='{0}' && this.sms_sent_count > 0".format(
        TODAY.strftime('%Y-%m-%d'))
    print(" Query : ", query)

    listOfUsersToSendSMS = inactive_user_coll.find({'$where': query})
    all_sms = []
    for user in listOfUsersToSendSMS:
        userData = users_coll.find_one({'_id': ObjectId(user['user'])}, {'email': 1, 'phoneNo': 1, 'name': 1})
        projectData = project_coll.find_one({'_id': ObjectId(user['project'])}, {'title': 1})
        all_sms.append({'user': userData, 'project': projectData})

    print(" List of emails that are going to be notified : ")

    for sms in all_sms:
        print(' SMS : ', sms['user'] is None)
        if sms['user'] is None or sms['project'] is None:
            continue

        link = '/user/login/redirect-dashboard/{0}/{1}'.format(encodingObjectId(sms['user']['_id']),
                                                               sms['project']['_id'])
        body = compose_mail(sms['user']['name'], sms['project']['title'], link)
        if 'phoneNo' in sms['user']:
            to = sms['user']['phoneNo']
            print(' SMS: ', {'to': to, 'body': body})
            time.sleep(10)
            # status = send_transactional_sms({'to': to, 'body': body})
            # if status:
            #     print(" SMS to ", to, " has been sent Successfully .... ")

    print(" Sending to Admin ....")
    ADMIN = '9108004352'
    # ADMIN = '8743086471'
    content = ' Alert! Routine(369) Notification Scheduled , notified ' + str(len(all_sms)) + " Users !"
    ADMIN_SMS = {'to': ADMIN, 'body': content}
    print(" Admin : ", ADMIN_SMS)
    send_transactional_sms(ADMIN_SMS)
    return True


if __name__ == "__main__":
    generateSMSFromList()
